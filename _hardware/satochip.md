---
title: SatoChip
appId: satochip
authors:
- kiwilamb
released: 
discontinued: 
updated: 
version: 
binaries: 
dimensions:
- 54
- 85
- 1
weight: 10
provider: SatoChip
providerWebsite: https://satochip.io
website: https://satochip.io
shop: https://satochip.io/shop/
country: BE
price: 25EUR
repository: 
issue: 
icon: satochip.png
bugbounty: 
meta: ok
verdict: noita
date: 2021-08-08
signer: 
reviewArchive: 
twitter: satochipwallet
social:
- https://www.linkedin.com/company/satochip
- https://github.com/Toporin

---

This hardware device lacks a screen or a button, this device cannot provide basic security of hardware wallets.