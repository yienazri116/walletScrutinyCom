---
title: ColdLar Touch
appId: coldlartouch
authors:
- kiwilamb
- felipe
released: 
discontinued: 
updated: 
version: 
binaries: 
dimensions:
- 54
- 86
- 0.86
weight: 6
provider: ColdLar Information Technology Co.
providerWebsite: https://www.coldlar.com/
website: https://www.coldlar.com/productDetails/10065
shop: https://www.coldlar.com/productDetails/10065
country: CH
price: 28.8USD
repository: 
issue: 
icon: coldlartouch.png
bugbounty: 
meta: ok
verdict: noita
date: 2021-07-11
signer: 
reviewArchive: 
twitter: Coldlar
social: 

---

Without a screen and whitout a button, this device cannot provide basic security of hardware wallets.