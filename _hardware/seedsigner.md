---
title: Seed Signer
appId: seedsigner
authors:
- danny
released: 2020-12-20
discontinued: 
updated: 2021-11-21
version: 0.4.5
binaries: 
dimensions: 
weight: 
provider: Seed Signer
providerWebsite: 
website: https://seedsigner.com/
shop: 
country: 
price: 50USD
repository: https://github.com/SeedSigner/seedsigner
issue: 
icon: seedsigner.png
bugbounty: 
meta: ok
verdict: wip
date: 2022-02-17
signer: 
reviewArchive: 
twitter: SeedSigner
social: 

---

The Seed Signer is a truly Open Source project that lowers the barrier for entry for airgapped multi-signature cryptocurrency hardware wallets. The code is publicly available as are the instructions for assembly. 

It claims to [solve the following problems](https://seedsigner.com/faqs/):

> - Creates a secure, air-gapped environment for private key generation
> - Enforces strict separation between private key storage and protocol software / internet
> - Lowers the barrier cost of multi-sig security (from several hundred to < $50)

## Can the private keys be created offline? 

Yes. The seed signer is airgapped.

## Are the private keys shared? 

No. The companion apps only get signed transactions and no keys.

## Does the device display the receive address for confirmation?

Yes. 

## Does the interface have a display screen and buttons which allows the user to confirm transaction details?

Yes. 

## Is it reproducible?

This product requires further verification.