---
title: Sugi
appId: sugi
authors:
- kiwilamb
released: 
discontinued: 
updated: 
version: 
binaries: 
dimensions:
- 54
- 85
- 1
weight: 
provider: Sugi
providerWebsite: https://sugi.io
website: https://sugi.io
shop: https://sugi.io/#pricing
country: BE
price: 59.9EUR
repository: 
issue: 
icon: sugi.png
bugbounty: 
meta: ok
verdict: noita
date: 2021-08-08
signer: 
reviewArchive: 
twitter: SugiCard
social:
- https://www.linkedin.com/company/sugi-card
- https://www.facebook.com/SugiCard

---

This hardware device lacks a screen or a button, this device cannot provide basic security of hardware wallets.