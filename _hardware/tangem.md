---
title: Tangem
appId: tangem
authors:
- kiwilamb
released: 
discontinued: 
updated: 
version: 
binaries: 
dimensions:
- 54
- 85
- 0.6
weight: 3
provider: Tangem
providerWebsite: https://tangem.com
website: https://shop.tangem.com/collections/crypto-hardware-wallet
shop: https://shop.tangem.com/products/tangem-card-pack
country: CH
price: 39.99USD
repository: 
issue: 
icon: tangem.png
bugbounty: 
meta: ok
verdict: noita
date: 2021-08-08
signer: 
reviewArchive: 
twitter: tangem
social:
- https://www.linkedin.com/company/tangem
- https://github.com/tangem

---

This hardware device lacks a screen or a button, this device cannot provide basic security of hardware wallets.