---
title: KeyCard
appId: keycard
authors:
- kiwilamb
released: 
discontinued: 
updated: 
version: 
binaries: 
dimensions:
- 54
- 85
- 1
weight: 10
provider: KeyCard
providerWebsite: https://keycard.tech
website: https://keycard.tech
shop: https://get.keycard.tech/
country: 
price: 24.9EUR
repository: https://github.com/status-im/status-keycard
issue: 
icon: keycard.png
bugbounty: 
meta: ok
verdict: noita
date: 2021-08-08
signer: 
reviewArchive: 
twitter: Keycard_
social: 

---

This hardware device lacks a screen or a button, this device cannot provide basic security of hardware wallets.