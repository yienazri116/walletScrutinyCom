---
wsId: 
title: Atari Smart Wallet
altTitle: 
authors:
- danny
users: 1000
appId: com.atari
appCountry: 
released: 2021-07-11
updated: 2021-11-13
version: 2.08.01
stars: 2.1935484
ratings: 183
reviews: 26
size: 59M
website: https://wallet.atarichain.com/
repository: 
issue: 
icon: com.atari.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-11-15
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 

---

## App Description

> Atari Smart Wallet enables you to manage and exchange your cryptocurrencies with Zero fees within the Atari Ecosystem. ATRI, ETH, USDT, BTC, LTC and BNB

## The Site

Much of the site's links to its social media pages and iOS app do not work.

## The App

We tried the app and it has no Bitcoin support. Clicking on the Bitcoin logo merely says "Comming Soon" (sic)

## Verdict

This app **does not have Bitcoin** support, at least not yet.
