---
wsId: 
title: 'Status: Crypto Wallet, Messenger, Ethereum Browser'
altTitle: 
authors:
- leo
users: 1000000
appId: im.status.ethereum
appCountry: 
released: 2020-02-05
updated: 2022-02-15
version: 1.18.2
stars: 4.3950615
ratings: 2069
reviews: 41
size: 81M
website: https://status.im
repository: 
issue: 
icon: im.status.ethereum.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2022-01-09
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 

---

This app appears to not support BTC.
