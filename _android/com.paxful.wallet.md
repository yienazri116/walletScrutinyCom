---
wsId: Paxful
title: Paxful Bitcoin & Crypto Wallet | Buy BTC ETH USDT
altTitle: 
authors:
- leo
users: 1000000
appId: com.paxful.wallet
appCountry: 
released: 2019-04-30
updated: 2022-03-05
version: 2.8.4.653
stars: 3.1993356
ratings: 21528
reviews: 1646
size: 28M
website: https://Paxful.com
repository: 
issue: 
icon: com.paxful.wallet.png
bugbounty: 
meta: ok
verdict: custodial
date: 2020-10-12
signer: 
reviewArchive: 
twitter: paxful
social:
- https://www.facebook.com/paxful
- https://www.reddit.com/r/paxful
redirect_from:
- /paxful/
- /com.paxful.wallet/
- /posts/2019/11/paxful/
- /posts/com.paxful.wallet/

---

According to their Playstore description:

> The bitcoin wallet app is also the ultimate companion tool to Paxful, one of
the world’s biggest peer-to-peer bitcoin marketplaces.

> Track your open trades on Paxful so you know the current status of your most
recent transactions as you buy and sell bitcoin

which sounds like a tool to manage coins on [paxful](https://paxful.com/).

Nowhere on the Playstore or on their website did we find a link to source code.

[Nowhere on GitHub](https://github.com/search?p=3&q=%22com.paxful.wallet%22) did
we find their applicationId `com.paxful.wallet` as an actual applicationId in
an Android project.

Our verdict thus is: **not verifiable** and probably custodial.
