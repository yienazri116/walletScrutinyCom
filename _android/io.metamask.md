---
wsId: metamask
title: MetaMask - Blockchain Wallet
altTitle: 
authors:
- leo
users: 10000000
appId: io.metamask
appCountry: 
released: 2020-09-01
updated: 2022-03-21
version: 4.2.2
stars: 4.580981
ratings: 57131
reviews: 2050
size: 194M
website: https://metamask.io
repository: 
issue: 
icon: io.metamask.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-05-01
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 

---

This is an ETH-only app and thus not a Bitcoin wallet.
