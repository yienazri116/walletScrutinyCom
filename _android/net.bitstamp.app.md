---
wsId: Bitstamp
title: Bitstamp – Crypto on the go
altTitle: 
authors:
- leo
users: 500000
appId: net.bitstamp.app
appCountry: 
released: 2019-01-29
updated: 2022-03-10
version: '3.5'
stars: 4.131579
ratings: 11088
reviews: 530
size: 141M
website: https://www.bitstamp.net
repository: 
issue: 
icon: net.bitstamp.app.png
bugbounty: 
meta: ok
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive: 
twitter: Bitstamp
social:
- https://www.linkedin.com/company/bitstamp
- https://www.facebook.com/Bitstamp
redirect_from: 

---

On the Google Play description we read:

> Convenient, but secure
>
> ● We store 98% of all crypto assets in cold storage

which means you don't get the keys for your coins. This is a custodial service
and therefore **not verifiable**.
