---
wsId: OKEx
title: 'OKX: Buy Bitcoin, ETH, Crypto'
altTitle: 
authors:
- leo
users: 1000000
appId: com.okinc.okex.gp
appCountry: 
released: 2019-10-29
updated: 2022-03-21
version: 6.0.16
stars: 4.598063
ratings: 188391
reviews: 725
size: 176M
website: https://www.okx.com/
repository: 
issue: 
icon: com.okinc.okex.gp.png
bugbounty: 
meta: ok
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive: 
twitter: OKEx
social:
- https://www.facebook.com/okexofficial
- https://www.reddit.com/r/OKEx
redirect_from:
- /com.okinc.okex.gp/
- /posts/com.okinc.okex.gp/

---

This app gives you access to a trading platform which sounds fully custodial and
therefore **not verifiable**.
