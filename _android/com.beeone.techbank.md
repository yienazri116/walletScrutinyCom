---
wsId: TECHBANK
title: TechBank
altTitle: 
authors:
- leo
users: 10000
appId: com.beeone.techbank
appCountry: 
released: 2019-07-14
updated: 2022-02-15
version: 4.9.11
stars: 4.1313133
ratings: 700
reviews: 43
size: 86M
website: https://techbank.finance/
repository: 
issue: 
icon: com.beeone.techbank.png
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-30
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from:
- /com.beeone.techbank/

---

They claim a million "members" and list big banks as their "acquirers" but there
is little to back those claims. 10k downloads on Play Store and eight ratings on
App Store don't look like a million users. The reviews on both platforms also
are abysmal.

Neither on the description nor their website do we find claims about this app being
a non-custodial wallet and as the name Tech**Bank** sounds rather custodial, we
file it as such and conclude this app is **not verifiable**.
