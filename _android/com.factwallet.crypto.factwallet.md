---
wsId: 
title: Fact Wallet - Bitcoin and cryptocurrency Wallet
altTitle: 
authors:
- leo
users: 100
appId: com.factwallet.crypto.factwallet
appCountry: 
released: 2020-10-23
updated: 2020-11-13
version: '1.4'
stars: 
ratings: 
reviews: 
size: 3.8M
website: https://factpocket.com/
repository: 
issue: 
icon: com.factwallet.crypto.factwallet.png
bugbounty: 
meta: stale
verdict: fewusers
date: 2021-11-09
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from:
- /com.factwallet.crypto.factwallet/

---

