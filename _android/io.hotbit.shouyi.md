---
wsId: hotbit
title: Hotbit
altTitle: 
authors:
- danny
users: 1000000
appId: io.hotbit.shouyi
appCountry: 
released: 2019-09-19
updated: 2022-03-03
version: 1.4.2
stars: 4.0599456
ratings: 75096
reviews: 1363
size: 20M
website: https://www.hotbit.io/
repository: 
issue: 
icon: io.hotbit.shouyi.png
bugbounty: 
meta: ok
verdict: custodial
date: 2021-08-27
signer: 
reviewArchive: 
twitter: Hotbit_news
social:
- https://www.linkedin.com/company/hotbitexchange
- https://www.facebook.com/hotbitexchange
redirect_from: 

---

Hotbit describes itself as

> a professional cryptocurrency trading platform

these usually tend to be custodial.

It laters mentions the use of hot and cold wallets, and both are usually traits of custodial offerings. We mark this as **not verifiable.**
