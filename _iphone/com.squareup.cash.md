---
wsId: CashApp
title: Cash App
altTitle: 
authors:
- leo
appId: com.squareup.cash
appCountry: 
idd: 711923939
released: 2013-10-16
updated: 2022-03-21
version: '3.61'
stars: 4.73129
reviews: 2061033
size: '257562624'
website: https://cash.app
repository: 
issue: 
icon: com.squareup.cash.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2020-12-22
signer: 
reviewArchive: 
twitter: cashapp
social: 

---

On their website the provider claims:

> **Coin Storage**<br>
  Your Bitcoin balance is securely stored in our offline system

which means it is custodial.
