---
wsId: BinanceTR
title: 'Binance TR: BTC | SHIB | DOGE'
altTitle: 
authors:
- danny
appId: com.binanceCloudTR.binance
appCountry: tr
idd: 1548636153
released: 2021-02-18
updated: 2022-03-20
version: 1.8.0
stars: 4.36211
reviews: 12836
size: '92229632'
website: https://www.trbinance.com/
repository: 
issue: 
icon: com.binanceCloudTR.binance.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive: 
twitter: BinanceTR
social:
- https://www.facebook.com/TRBinanceTR

---

{% include copyFromAndroid.html %}
