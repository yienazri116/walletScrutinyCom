---
wsId: goabra
title: 'Abra: Buy Bitcoin & Earn Yield'
altTitle: 
authors:
- leo
appId: com.goabra.abra
appCountry: 
idd: 966301394
released: 2015-03-12
updated: 2022-03-18
version: 118.1.0
stars: 4.57122
reviews: 17629
size: '121060352'
website: 
repository: 
issue: 
icon: com.goabra.abra.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-01-04
signer: 
reviewArchive: 
twitter: AbraGlobal
social:
- https://www.linkedin.com/company/abra
- https://www.facebook.com/GoAbraGlobal

---

This is the iPhone version of the Android
{% include walletLink.html wallet='android/com.plutus.wallet' %}.

Just like the Android version, this wallet is **not verifiable**.
