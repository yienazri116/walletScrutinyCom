---
wsId: bitpaywallet
title: BitPay - Bitcoin Wallet & Card
altTitle: 
authors:
- leo
appId: com.bitpay.wallet
appCountry: 
idd: 1149581638
released: 2016-10-24
updated: 2022-03-04
version: 12.11.4
stars: 3.98859
reviews: 1402
size: '88250368'
website: https://bitpay.com
repository: 
issue: 
icon: com.bitpay.wallet.jpg
bugbounty: 
meta: ok
verdict: ftbfs
date: 2021-04-27
signer: 
reviewArchive: 
twitter: BitPay
social:
- https://www.linkedin.com/company/bitpay-inc-
- https://www.facebook.com/BitPayOfficial

---

{% include copyFromAndroid.html %}
