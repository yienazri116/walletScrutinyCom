---
wsId: ember
title: Ember Fund - Invest in Crypto
altTitle: 
authors:
- danny
appId: com.emberfund.ember
appCountry: us
idd: 1406211993
released: 2018-08-04
updated: 2022-03-17
version: '31.14'
stars: 4.43133
reviews: 568
size: '119131136'
website: https://emberfund.io/
repository: 
issue: 
icon: com.emberfund.ember.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: Ember_Fund
social:
- https://github.com/ember-fund

---

{% include copyFromAndroid.html %}