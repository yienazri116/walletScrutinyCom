---
wsId: monnos
title: Monnos | Buy Bitcoin
altTitle: 
authors:
- danny
appId: com.monnos
appCountry: br
idd: 1476884342
released: 2019-09-30
updated: 2022-03-16
version: 5.4.0
stars: 4.58011
reviews: 181
size: '157171712'
website: https://monnos.com
repository: 
issue: 
icon: com.monnos.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: monnosGlobal
social:
- https://www.linkedin.com/company/monnosglobal
- https://www.facebook.com/MonnosGlobal

---

{% include copyFromAndroid.html %}