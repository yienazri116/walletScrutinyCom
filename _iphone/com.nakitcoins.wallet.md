---
wsId: NakitCoins
title: NakitCoins
altTitle: 
authors:
- danny
appId: com.nakitcoins.wallet
appCountry: us
idd: 1559751218
released: 2021-04-21
updated: 2022-02-15
version: 2.1.3
stars: 5
reviews: 2
size: '9615360'
website: https://nakitcoins.com
repository: 
issue: 
icon: com.nakitcoins.wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-22
signer: 
reviewArchive: 
twitter: nakitcoins
social:
- https://www.facebook.com/NakitCoins

---

{% include copyFromAndroid.html %}