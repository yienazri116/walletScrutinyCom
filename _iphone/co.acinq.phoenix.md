---
wsId: phoenix
title: Phoenix Wallet
altTitle: 
authors:
- leo
- danny
appId: co.acinq.phoenix
appCountry: us
idd: 1544097028
released: 2021-07-13
updated: 2022-03-15
version: 1.3.2
stars: 4.33333
reviews: 12
size: '23897088'
website: https://phoenix.acinq.co
repository: https://github.com/ACINQ/phoenix-kmm
issue: 
icon: co.acinq.phoenix.jpg
bugbounty: 
meta: ok
verdict: nonverifiable
date: 2021-10-01
signer: 
reviewArchive: 
twitter: PhoenixWallet
social: 

---

{% include copyFromAndroid.html %}
