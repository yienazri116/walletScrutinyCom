---
wsId: gopax
title: GOPAX
altTitle: 
authors:
- danny
appId: kr.co.gopax
appCountry: kr
idd: 1369896843
released: 2018-06-21
updated: 2022-03-04
version: 1.8.3
stars: 2.82688
reviews: 439
size: '53275648'
website: https://www.gopax.co.kr/notice
repository: 
issue: 
icon: kr.co.gopax.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: 
social: 

---

 {% include copyFromAndroid.html %}
