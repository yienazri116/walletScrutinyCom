---
wsId: bitcointrade
title: BitcoinTrade - Criptomoedas
altTitle: 
authors:
- danny
appId: com.root.BitcoinTrade
appCountry: br
idd: 1320032339
released: 2017-12-13
updated: 2022-03-21
version: 4.2.1
stars: 3.80584
reviews: 788
size: '37530624'
website: http://www.bitcointrade.com.br/
repository: 
issue: 
icon: com.root.BitcoinTrade.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-11
signer: 
reviewArchive: 
twitter: 
social:
- https://www.linkedin.com/company/bitcointrade
- https://www.facebook.com/BitcointradeBR

---

{% include copyFromAndroid.html %}
