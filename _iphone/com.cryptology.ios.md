---
wsId: Cryptology
title: 'Cryptology: Blockchain Wallet'
altTitle: 
authors:
- danny
appId: com.cryptology.ios
appCountry: gb
idd: 1313186415
released: 2018-03-23
updated: 2022-03-20
version: 3.9.2
stars: 5
reviews: 3
size: '58217472'
website: http://cryptology.com
repository: 
issue: 
icon: com.cryptology.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-08
signer: 
reviewArchive: 
twitter: Cryptologyexch
social:
- https://www.facebook.com/Cryptologyexch

---

{% include copyFromAndroid.html %}
