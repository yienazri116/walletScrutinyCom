---
wsId: dcoin
title: Dcoin - Trade Bitcoin,Ethereum
altTitle: 
authors:
- danny
appId: com.dcoin.ios
appCountry: us
idd: 1508064925
released: 2018-12-20
updated: 2022-03-22
version: 5.2.3
stars: 3.3125
reviews: 16
size: '88032256'
website: https://www.dcoin.com/
repository: 
issue: 
icon: com.dcoin.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive: 
twitter: dcoinexchange
social:
- https://www.linkedin.com/company/dcoin-exchange

---

 {% include copyFromAndroid.html %}
