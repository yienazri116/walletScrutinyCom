---
wsId: salletone
title: SalletOne Live
altTitle: 
authors:
- danny
appId: com.sallet.walletBit
appCountry: us
idd: 1577493312
released: 2021-07-29
updated: 2022-03-22
version: 1.9.1
stars: 5
reviews: 2
size: '16143360'
website: https://www.salletone.com
repository: 
issue: 
icon: com.sallet.walletBit.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2022-02-17
signer: 
reviewArchive: 
twitter: salletone
social:
- https://www.facebook.com/salletone
- https://github.com/SalletOne

---

{% include copyFromAndroid.html %}
