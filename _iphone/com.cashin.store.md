---
wsId: BitcoinPoint
title: BitcoinPoint
altTitle: 
authors:
- danny
appId: com.cashin.store
appCountry: gb
idd: 1363753409
released: 2018-08-15
updated: 2022-02-04
version: '4.9'
stars: 3.95
reviews: 20
size: '78316544'
website: https://bitcoinpoint.com
repository: 
issue: 
icon: com.cashin.store.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-17
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}
