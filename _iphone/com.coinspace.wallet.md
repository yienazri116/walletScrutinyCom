---
wsId: coinspace
title: 'Coin Wallet: Buy Bitcoin'
altTitle: 
authors:
- leo
appId: com.coinspace.wallet
appCountry: 
idd: 980719434
released: 2015-12-14
updated: 2022-03-10
version: 5.1.6
stars: 4.43262
reviews: 141
size: '40205312'
website: https://coin.space/
repository: https://github.com/CoinSpace/CoinSpace
issue: 
icon: com.coinspace.wallet.jpg
bugbounty: https://www.openbugbounty.org//bugbounty/CoinAppWallet/
meta: ok
verdict: ftbfs
date: 2020-12-20
signer: 
reviewArchive: 
twitter: coinappwallet
social:
- https://www.linkedin.com/company/coin-space
- https://www.facebook.com/coinappwallet

---

{% include copyFromAndroid.html %}
