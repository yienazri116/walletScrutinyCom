---
wsId: hoo
title: Hoo
altTitle: 
authors:
- danny
appId: cn.maolian.hufu.app.standard
appCountry: us
idd: 1387872759
released: 2018-06-28
updated: 2022-03-17
version: 4.6.71
stars: 3.30233
reviews: 86
size: '154031104'
website: https://hoo.com
repository: 
issue: 
icon: cn.maolian.hufu.app.standard.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-08
signer: 
reviewArchive: 
twitter: Hoo_exchange
social:
- https://www.facebook.com/hooexchange
- https://www.reddit.com/r/HooExchange

---

{% include copyFromAndroid.html %}
