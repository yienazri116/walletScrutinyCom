---
wsId: bitcointradingcapital
title: Bitcoin trading - Capital.com
altTitle: 
authors:
- danny
appId: com.kapital.trade.crypto
appCountry: cz
idd: 1487443266
released: 2019-11-26
updated: 2022-03-18
version: 1.41.4
stars: 4.7602
reviews: 784
size: '79345664'
website: https://expcapital.com
repository: 
issue: 
icon: com.kapital.trade.crypto.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-09-03
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}
