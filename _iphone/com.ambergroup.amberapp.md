---
wsId: ambercrypto
title: 'WhaleFin: Buy Crypto & Bitcoin'
altTitle: 
authors:
- danny
appId: com.ambergroup.amberapp
appCountry: us
idd: 1515652068
released: 2020-09-21
updated: 2022-03-18
version: 2.2.0
stars: 4.7962
reviews: 368
size: '235167744'
website: https://www.whalefin.com
repository: 
issue: 
icon: com.ambergroup.amberapp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-11
signer: 
reviewArchive: 
twitter: ambergroup_io
social:
- https://www.linkedin.com/company/amberbtc
- https://www.facebook.com/ambergroup.io

---

{% include copyFromAndroid.html %}
