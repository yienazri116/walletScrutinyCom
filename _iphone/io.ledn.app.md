---
wsId: Ledn
title: Ledn
altTitle: 
authors:
- danny
appId: io.ledn.app
appCountry: ca
idd: 1543035976
released: 2021-01-20
updated: 2022-02-24
version: 0.3.0
stars: 4.83333
reviews: 12
size: '24001536'
website: https://ledn.io
repository: 
issue: 
icon: io.ledn.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-02-27
signer: 
reviewArchive: 
twitter: hodlwithLedn
social:
- https://www.linkedin.com/company/ledn-inc
- https://www.facebook.com/Ledn.io

---

{% include copyFromAndroid.html %}
