---
wsId: PionexLite
title: Pionex - Buy BTC and ETH
altTitle: 
authors:
- danny
appId: com.pionex.lite.appstore
appCountry: kr
idd: 1567213944
released: 2021-05-22
updated: 2021-08-27
version: 1.1.7
stars: 
reviews: 
size: 77867008
website: 
repository: 
issue: 
icon: com.pionex.lite.appstore.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-30
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}