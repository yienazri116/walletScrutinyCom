---
wsId: fibermode
title: 'Mode: Buy, Earn & Grow Bitcoin'
altTitle: 
authors:
- danny
appId: com.fibermode.Mode-Wallet
appCountry: gb
idd: 1483284435
released: 2019-11-26
updated: 2022-02-04
version: 5.3.7
stars: 4.30556
reviews: 972
size: '44493824'
website: https://www.modeapp.com
repository: 
issue: 
icon: com.fibermode.Mode-Wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive: 
twitter: modeapp_
social:
- https://www.linkedin.com/company/modeapp-com
- https://www.facebook.com/themodeapp

---

{% include copyFromAndroid.html %}
