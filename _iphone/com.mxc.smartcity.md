---
wsId: datadash
title: DataDash App
altTitle: 
authors:
- danny
appId: com.mxc.smartcity
appCountry: us
idd: 1509218470
released: 2020-06-30
updated: 2022-03-01
version: 2.0.7
stars: 4.18293
reviews: 82
size: '119764992'
website: https://www.mxc.org
repository: 
issue: 
icon: com.mxc.smartcity.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-10-01
signer: 
reviewArchive: 
twitter: mxcfoundation
social:
- https://www.facebook.com/MXCfoundation
- https://www.reddit.com/r/MXC_Foundation

---

{% include copyFromAndroid.html %}
