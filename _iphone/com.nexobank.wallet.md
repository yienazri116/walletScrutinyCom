---
wsId: nexo
title: 'Nexo: Buy BTC, ETH, SOL, AVAX'
altTitle: 
authors:
- leo
appId: com.nexobank.wallet
appCountry: 
idd: 1455341917
released: 2019-06-30
updated: 2022-03-05
version: 2.2.21
stars: 3.57519
reviews: 798
size: '56515584'
website: https://nexo.io
repository: 
issue: 
icon: com.nexobank.wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-22
signer: 
reviewArchive: 
twitter: NexoFinance
social:
- https://www.facebook.com/nexofinance
- https://www.reddit.com/r/Nexo

---

In the description on the App Store we read:

> • Your Nexo account is secured through BitGo, the leader in crypto
  custodianship, and benefits from top-tier insurance.

which makes it a custodial app. The custodian is claimed to be "BitGo" so as a
user you have to trust BitGo to not lose the coins and Nexo to actually not hold
all or part of the coins. In any case this app is **not verifiable**.
