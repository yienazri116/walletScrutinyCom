---
wsId: frontierDeFi
title: 'Frontier: Crypto & DeFi Wallet'
altTitle: 
authors:
- danny
appId: com.frontierwallet
appCountry: in
idd: 1482380988
released: 2019-11-05
updated: 2022-03-10
version: 4.3.0
stars: 5
reviews: 26
size: '110660608'
website: https://frontier.xyz
repository: 
issue: 
icon: com.frontierwallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2022-01-13
signer: 
reviewArchive: 
twitter: FrontierDotXYZ
social: 

---

{% include copyFromAndroid.html %}
