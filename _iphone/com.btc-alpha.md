---
wsId: BTCAlpha
title: 'BTC-Alpha: Buy Sell Bitcoin'
altTitle: 
authors:
- danny
appId: com.btc-alpha
appCountry: us
idd: 1437629304
released: 2019-04-20
updated: 2022-02-21
version: 1.12.13
stars: 4.27273
reviews: 11
size: '89218048'
website: https://btc-alpha.com
repository: 
issue: 
icon: com.btc-alpha.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive: 
twitter: btcalpha
social:
- https://www.linkedin.com/company/btcalpha
- https://www.facebook.com/btcalpha

---

{% include copyFromAndroid.html %}


