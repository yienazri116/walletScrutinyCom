---
wsId: StartEOSWallet
title: Start Wallet（starteos）
altTitle: 
authors:
- danny
appId: com.HCLT.Starteos.store
appCountry: us
idd: 1441373609
released: 2019-01-21
updated: 2022-02-15
version: 02.00.21
stars: 4.57143
reviews: 7
size: '107711488'
website: http://starteos.io
repository: 
issue: 
icon: com.HCLT.Starteos.store.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-11-22
signer: 
reviewArchive: 
twitter: Starteos
social:
- https://www.facebook.com/starteos.io.7
- https://github.com/StarteosIO

---

{% include copyFromAndroid.html %}
