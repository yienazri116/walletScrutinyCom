---
wsId: voyager
title: 'Voyager: Buy BTC, SHIB, ETH'
altTitle: 
authors:
- leo
appId: com.investvoyager.voyager-ios
appCountry: 
idd: 1396178579
released: 2019-02-13
updated: 2022-03-15
version: 3.2.2
stars: 4.74238
reviews: 98496
size: '90024960'
website: https://www.investvoyager.com/
repository: 
issue: 
icon: com.investvoyager.voyager-ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-01-02
signer: 
reviewArchive: 
twitter: investvoyager
social:
- https://www.linkedin.com/company/investvoyager
- https://www.facebook.com/InvestVoyager
- https://www.reddit.com/r/Invest_Voyager

---

On their website we read:

> **Advanced Security**<br>
  Offline storage, advanced fraud protection, and government-regulated processes
  keep your assets secure and your personal information safe.

which means this is a custodial offering and therefore **not verifiable**.
