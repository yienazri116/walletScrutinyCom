---
wsId: LBank
title: LBank - Buy Bitcoin & Crypto
altTitle: 
authors:
- danny
appId: com.LBank.LBankNavApp
appCountry: us
idd: 1437346368
released: 2019-02-22
updated: 2022-03-17
version: 4.9.23
stars: 4.09976
reviews: 421
size: '224046080'
website: https://www.lbank.info/
repository: 
issue: 
icon: com.LBank.LBankNavApp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive: 
twitter: LBank_Exchange
social:
- https://www.linkedin.com/company/lbank
- https://www.facebook.com/LBank.info

---

{% include copyFromAndroid.html %}
