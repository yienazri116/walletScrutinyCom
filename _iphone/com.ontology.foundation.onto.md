---
wsId: ONTO
title: ONTO-Cross-chain Crypto Wallet
altTitle: 
authors:
- danny
appId: com.ontology.foundation.onto
appCountry: us
idd: 1436009823
released: 2018-09-21
updated: 2022-03-20
version: 4.2.1
stars: 4.17807
reviews: 73
size: '231945216'
website: https://www.onto.app
repository: 
issue: 
icon: com.ontology.foundation.onto.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-09-15
signer: 
reviewArchive: 
twitter: ONTOWallet
social: 

---

{% include copyFromAndroid.html %}