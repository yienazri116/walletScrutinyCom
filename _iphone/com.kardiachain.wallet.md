---
wsId: kardiawallet
title: KardiaChain Wallet
altTitle: 
authors:
- danny
appId: com.kardiachain.wallet
appCountry: vn
idd: 1551620695
released: 2021-03-02
updated: 2022-03-04
version: 2.4.4
stars: 4.38571
reviews: 70
size: '56050688'
website: https://kardiachain.io/
repository: 
issue: 
icon: com.kardiachain.wallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-10-05
signer: 
reviewArchive: 
twitter: KardiaChain
social:
- https://www.facebook.com/KardiaChainFoundation

---

{% include copyFromAndroid.html %}
