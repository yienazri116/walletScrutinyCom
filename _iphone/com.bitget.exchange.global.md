---
wsId: Bitget
title: Bitget - Trade BTC, EOS, XRP
altTitle: 
authors:
- danny
appId: com.bitget.exchange.global
appCountry: ua
idd: 1442778704
released: 2018-11-29
updated: 2022-03-14
version: 1.2.33
stars: 5
reviews: 2
size: '127051776'
website: https://www.bitget.com/en
repository: 
issue: 
icon: com.bitget.exchange.global.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-17
signer: 
reviewArchive: 
twitter: bitgetglobal
social:
- https://www.linkedin.com/company/bitget
- https://www.facebook.com/BitgetGlobal

---

 {% include copyFromAndroid.html %}
