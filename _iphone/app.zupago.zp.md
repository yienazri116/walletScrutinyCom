---
wsId: ZuPago
title: ZuPago
altTitle: 
authors:
- danny
appId: app.zupago.zp
appCountry: us
idd: 1565673730
released: 2021-05-10
updated: 2022-02-06
version: 1.0.48
stars: 4.61364
reviews: 44
size: '98862080'
website: https://zupago.app
repository: 
issue: 
icon: app.zupago.zp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-12-19
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}