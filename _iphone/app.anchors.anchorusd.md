---
wsId: AnchorUSD
title: Anchor - Buy and Send Crypto
altTitle: 
authors:
- danny
appId: app.anchors.anchorusd
appCountry: us
idd: 1495986023
released: 2020-01-30
updated: 2022-01-19
version: 1.17.9
stars: 4.3453
reviews: 5123
size: '36886528'
website: https://www.tryanchor.com/
repository: 
issue: 
icon: app.anchors.anchorusd.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: anchorusd
social: 

---

{% include copyFromAndroid.html %}
