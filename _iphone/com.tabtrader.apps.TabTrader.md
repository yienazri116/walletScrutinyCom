---
wsId: tabtrader
title: TabTrader - crypto terminal
altTitle: 
authors:
- leo
- kiwilamb
- danny
appId: com.tabtrader.apps.TabTrader
appCountry: 
idd: 1095716562
released: 2016-09-02
updated: 2021-11-20
version: 3.1.2
stars: 4.73898
reviews: 3992
size: '26054656'
website: https://tab-trader.com
repository: 
issue: 
icon: com.tabtrader.apps.TabTrader.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-04-17
signer: 
reviewArchive: 
twitter: tabtraderpro
social:
- https://www.linkedin.com/company/tabtrader
- https://www.facebook.com/tabtrader

---

{% include copyFromAndroid.html %}