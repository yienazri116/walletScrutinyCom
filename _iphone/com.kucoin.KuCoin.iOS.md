---
wsId: kucoin
title: KuCoin- Buy Bitcoin & Crypto
altTitle: 
authors:
- leo
appId: com.kucoin.KuCoin.iOS
appCountry: 
idd: 1378956601
released: 2018-05-14
updated: 2022-03-15
version: 3.52.2
stars: 4.60696
reviews: 12874
size: '187335680'
website: 
repository: 
issue: 
icon: com.kucoin.KuCoin.iOS.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-02-09
signer: 
reviewArchive: 
twitter: KuCoinCom
social:
- https://www.linkedin.com/company/kucoin
- https://www.facebook.com/KuCoinOfficial
- https://www.reddit.com/r/kucoin

---

> KuCoin is the most popular bitcoin exchange that you can buy and sell bitcoin
  securely.

This app is the interface to an exchange. Exchanges are all custodial which
makes the app **not verifiable**.
