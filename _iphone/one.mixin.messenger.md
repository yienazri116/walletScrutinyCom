---
wsId: Mixin
title: Mixin Messenger
altTitle: 
authors:
- danny
- emanuel
- leo
appId: one.mixin.messenger
appCountry: us
idd: 1322324266
released: 2018-01-20
updated: 2022-03-04
version: 0.35.2
stars: 4.79623
reviews: 265
size: '66436096'
website: https://mixinmessenger.zendesk.com/
repository: https://github.com/MixinNetwork/android-app
issue: https://github.com/MixinNetwork/android-app/issues/2559
icon: one.mixin.messenger.jpg
bugbounty: 
meta: ok
verdict: ftbfs
date: 2021-11-17
signer: 
reviewArchive: 
twitter: MixinMessenger
social:
- https://www.facebook.com/MixinNetwork
- https://www.reddit.com/r/mixin

---

{% include copyFromAndroid.html %}
