---
wsId: bitazza
title: 'Bitazza: Bitcoin Exchange'
altTitle: 
authors:
- danny
appId: com.bitazza.ios
appCountry: th
idd: 1476944844
released: 2020-05-25
updated: 2022-02-28
version: 2.0.2
stars: 3.89167
reviews: 600
size: '51931136'
website: https://www.bitazza.com
repository: 
issue: 
icon: com.bitazza.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: bitazzaofficial
social:
- https://www.linkedin.com/company/bitazza
- https://www.facebook.com/bitazza

---

{% include copyFromAndroid.html %}
