---
wsId: RioWallet
title: RioDeFi Wallet
altTitle: 
authors:
- danny
appId: com.riodefi
appCountry: us
idd: 1560789648
released: 2021-05-13
updated: 2022-03-10
version: 2.5.0
stars: 5
reviews: 3
size: '69021696'
website: https://riodefi.com/
repository: 
issue: 
icon: com.riodefi.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-11-15
signer: 
reviewArchive: 
twitter: riodefiofficial
social:
- https://www.linkedin.com/company/riodefiofficial
- https://www.facebook.com/riodefiofficial

---

{% include copyFromAndroid.html %}
