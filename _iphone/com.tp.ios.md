---
wsId: TokenPocket
title: TokenPocket:Crypto&DeFi Wallet
altTitle: 
authors:
- leo
appId: com.tp.ios
appCountry: 
idd: 1436028697
released: 2018-09-23
updated: 2022-03-13
version: 1.7.8
stars: 3.42553
reviews: 235
size: '126656512'
website: https://www.tokenpocket.pro
repository: https://github.com/TP-Lab/tp-ios
issue: https://github.com/TP-Lab/tp-ios/issues/1
icon: com.tp.ios.jpg
bugbounty: 
meta: ok
verdict: obfuscated
date: 2022-01-09
signer: 
reviewArchive: 
twitter: TokenPocket_TP
social:
- https://www.facebook.com/TokenPocket

---

{% include copyFromAndroid.html %}
