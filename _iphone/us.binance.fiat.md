---
wsId: BinanceUS
title: '‎Binance.US: BTC, ETH & Crypto'
altTitle: 
authors:
- leo
appId: us.binance.fiat
appCountry: 
idd: 1492670702
released: 2020-01-05
updated: 2022-03-17
version: 2.9.6
stars: 4.20952
reviews: 96123
size: '221991936'
website: https://www.binance.us/en/home
repository: 
issue: 
icon: us.binance.fiat.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-01-10
signer: 
reviewArchive: 
twitter: binanceus
social:
- https://www.linkedin.com/company/binance-us
- https://www.facebook.com/BinanceUS

---

This is the iPhone version of {% include walletLink.html wallet='android/com.binance.us' %} and we
come to the same conclusion for the same reasons. This app is **not verifiable**.
