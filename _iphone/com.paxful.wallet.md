---
wsId: Paxful
title: Paxful | Marketplace & Wallet
altTitle: 
authors:
- leo
appId: com.paxful.wallet
appCountry: 
idd: 1443813253
released: 2019-05-09
updated: 2022-03-01
version: 2.7.9
stars: 3.663
reviews: 2362
size: '60800000'
website: https://paxful.com
repository: 
issue: 
icon: com.paxful.wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-14
signer: 
reviewArchive: 
twitter: paxful
social:
- https://www.facebook.com/paxful
- https://www.reddit.com/r/paxful

---

In the App Store description we can read:

> **A TRUSTED WALLET**<br>
  Safely store your crypto in your own personal mobile cryptocurrency wallet,
  which you’ll receive for free upon creating your account. Enable two-factor
  authentication for an added layer of protection. Take it everywhere you go and
  check your balance any time.

which really doesn't say much about who is actually holding the bitcoins.

On the website we found:

> **Get a free wallet**<br>
  Get a life-time free Bitcoin wallet maintained by BitGo, the leading provider
  of secure Bitcoin wallets.

which tells us this provider delegates custody to BitGo: BitGo is one of the
major custodian for exchanges and other services in the space.

As a custodial offering this app is **not verifiable**.
