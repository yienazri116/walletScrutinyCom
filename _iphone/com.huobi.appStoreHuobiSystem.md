---
wsId: huobi
title: Huobi - Buy & Sell Bitcoin
altTitle: 
authors:
- leo
appId: com.huobi.appStoreHuobiSystem
appCountry: 
idd: 1023263342
released: 2015-08-19
updated: 2022-03-20
version: 6.8.5
stars: 4.51926
reviews: 2155
size: '361535488'
website: http://www.hbg.com
repository: 
issue: 
icon: com.huobi.appStoreHuobiSystem.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-14
signer: 
reviewArchive: 
twitter: HuobiGlobal
social:
- https://www.facebook.com/huobiglobalofficial

---

Neither on Google Play nor on their website can we find a claim of a
non-custodial part to this app. We assume it is a purely custodial interface to
the exchange of same name and therefore **not verifiable**.
