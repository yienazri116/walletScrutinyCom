---
wsId: 2cash
title: 2cash
altTitle: 
authors:
- danny
appId: com.voicapps.app2cash-ios
appCountry: us
idd: 1530672224
released: 2020-12-01
updated: 2020-12-01
version: 1.2.4
stars: 5
reviews: 1
size: '81442816'
website: https://www.2cash.io
repository: 
issue: 
icon: com.voicapps.app2cash-ios.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2021-11-30
signer: 
reviewArchive: 
twitter: 2cashnetwork
social:
- https://www.linkedin.com/company/2cash
- https://www.facebook.com/2cashnetwork

---

{% include copyFromAndroid.html %}
