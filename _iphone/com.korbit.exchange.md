---
wsId: korbit
title: korbit
altTitle: 
authors:
- danny
appId: com.korbit.exchange
appCountry: us
idd: 1434511619
released: 2018-10-18
updated: 2022-03-17
version: 4.6.1
stars: 3.07143
reviews: 14
size: '144931840'
website: http://www.korbit.co.kr
repository: 
issue: 
icon: com.korbit.exchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-10
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}
