---
wsId: Bitmama
title: Bitmama
altTitle: 
authors:
- danny
appId: com.bitmama.bitmama.ios
appCountry: us
idd: 1561857024
released: 2021-06-30
updated: 2022-03-22
version: 1.0.31
stars: 0
reviews: 0
size: '72140800'
website: https://www.bitmama.io/
repository: 
issue: 
icon: com.bitmama.bitmama.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-10
signer: 
reviewArchive: 
twitter: bitmama
social:
- https://www.facebook.com/bitmama

---

{% include copyFromAndroid.html %}
