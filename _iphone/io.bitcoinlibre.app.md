---
wsId: BitcoinLibre
title: Bitcoin Libre
altTitle: 
authors:
- danny
appId: io.bitcoinlibre.app
appCountry: us
idd: 1590680702
released: 2021-10-27
updated: 2022-03-17
version: 2.1.3
stars: 4.66866
reviews: 166
size: '33367040'
website: https://libre.org
repository: 
issue: 
icon: io.bitcoinlibre.app.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-11-11
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}
