---
wsId: Forexcom
title: 'FOREX.com: CFD & Forex Trading'
altTitle: 
authors:
- danny
appId: com.gaincapital.forex
appCountry: gb
idd: 1506581586
released: 2020-10-14
updated: 2022-03-01
version: 1.91.2536
stars: 3.71429
reviews: 35
size: '90552320'
website: https://www.forex.com/en-uk/
repository: 
issue: 
icon: com.gaincapital.forex.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-10-16
signer: 
reviewArchive: 
twitter: forexcom
social:
- https://www.facebook.com/FOREXcom

---

{% include copyFromAndroid.html %}

