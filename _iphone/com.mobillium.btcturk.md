---
wsId: BtcTurk
title: BtcTurk| Bitcoin(BTC) Buy/Sell
altTitle: 
authors:
- danny
appId: com.mobillium.btcturk
appCountry: tr
idd: 1503482896
released: 2020-04-09
updated: 2022-03-08
version: 1.15.1
stars: 4.64091
reviews: 24022
size: '182249472'
website: https://www.btcturk.com
repository: 
issue: 
icon: com.mobillium.btcturk.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-10
signer: 
reviewArchive: 
twitter: btcturk
social:
- https://www.linkedin.com/company/btcturk
- https://www.facebook.com/btcturk

---

{% include copyFromAndroid.html %}
