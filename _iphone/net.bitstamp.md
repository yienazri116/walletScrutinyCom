---
wsId: Bitstamp
title: Bitstamp – Crypto on the go
altTitle: 
authors:
- leo
appId: net.bitstamp
appCountry: 
idd: 1406825640
released: 2019-01-30
updated: 2022-03-10
version: '3.5'
stars: 4.77177
reviews: 4916
size: '97803264'
website: https://www.bitstamp.net/
repository: 
issue: 
icon: net.bitstamp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-01-23
signer: 
reviewArchive: 
twitter: Bitstamp
social:
- https://www.linkedin.com/company/bitstamp
- https://www.facebook.com/Bitstamp

---

Just like on Play Store {% include walletLink.html wallet='android/net.bitstamp.app' %}, they claim:

> Convenient, but secure<br>
  ● We store 98% of all crypto assets in cold storage

which means you don't get the keys for your coins. This is a custodial service
and therefore **not verifiable**.
