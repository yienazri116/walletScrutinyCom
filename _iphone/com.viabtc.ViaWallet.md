---
wsId: ViaWallet
title: ViaWallet - Multi-chain Wallet
altTitle: 
authors:
- leo
appId: com.viabtc.ViaWallet
appCountry: 
idd: 1462031389
released: 2019-05-21
updated: 2022-01-06
version: 2.8.2
stars: 4.02632
reviews: 38
size: '101497856'
website: https://viawallet.com
repository: 
issue: 
icon: com.viabtc.ViaWallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: viawallet
social:
- https://www.facebook.com/ViaWallet

---

{% include copyFromAndroid.html %}
