---
wsId: Paritex
title: Paritex Bitcoin&Altcoin Trade
altTitle: 
authors:
- danny
appId: com.paritex.paritexapp
appCountry: tr
idd: 1550831461
released: 2021-02-11
updated: 2022-03-21
version: 2.3.7
stars: 4.38961
reviews: 77
size: '105248768'
website: https://www.paritex.com/
repository: 
issue: 
icon: com.paritex.paritexapp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-24
signer: 
reviewArchive: 
twitter: paritexexchange
social: 

---

{% include copyFromAndroid.html %}
