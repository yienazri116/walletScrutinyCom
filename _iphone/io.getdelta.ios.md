---
wsId: getDelta
title: Delta Investment Tracker
altTitle: 
authors: 
appId: io.getdelta.ios
appCountry: us
idd: 1288676542
released: 2017-09-25
updated: 2022-03-14
version: 2022.2.0
stars: 4.74699
reviews: 9869
size: '88723456'
website: https://delta.app
repository: 
issue: 
icon: io.getdelta.ios.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-11-01
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}
