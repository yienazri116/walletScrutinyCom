---
wsId: Sylo
title: Sylo
altTitle: 
authors:
- leo
appId: io.sylo.dapp
appCountry: 
idd: 1452964749
released: 2019-09-10
updated: 2022-02-11
version: 3.1.40
stars: 4.87671
reviews: 73
size: '199532544'
website: https://www.sylo.io/wallet/
repository: 
issue: 
icon: io.sylo.dapp.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: sylo
social:
- https://www.linkedin.com/company/sylo.io
- https://www.facebook.com/sylo.io
- https://www.reddit.com/r/sylo_io

---

{% include copyFromAndroid.html %}
