---
wsId: CardWalletCardano
title: CardWallet - ADA Crypto Wallet
altTitle: 
authors:
- danny
appId: fi.cardwallet
appCountry: pt
idd: 1578905885
released: 2021-09-28
updated: 2022-02-10
version: '2.1'
stars: 5
reviews: 2
size: '96854016'
website: https://cardwallet.fi/
repository: 
issue: 
icon: fi.cardwallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-11-11
signer: 
reviewArchive: 
twitter: CardWallet_fi
social: 

---

{% include copyFromAndroid.html %}
