---
wsId: AdmiralMarkets
title: Admirals
altTitle: 
authors:
- danny
appId: com.admiralmarkets.tradersroom
appCountry: us
idd: 1222861799
released: 2017-06-28
updated: 2022-03-17
version: 5.8.1
stars: 5
reviews: 9
size: '61928448'
website: https://admiralmarkets.com/
repository: 
issue: 
icon: com.admiralmarkets.tradersroom.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-10-16
signer: 
reviewArchive: 
twitter: AdmiralsGlobal
social:
- https://www.linkedin.com/company/-admiral-markets-group
- https://www.facebook.com/AdmiralsGlobal

---

{% include copyFromAndroid.html %}

