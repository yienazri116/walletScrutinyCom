---
wsId: CointoPayWallet
title: CTP Wallet
altTitle: 
authors:
- danny
appId: com.cointopay.iosapp
appCountry: us
idd: 1450194783
released: 2019-01-25
updated: 2021-03-31
version: 1.4.2
stars: 5
reviews: 1
size: '35804160'
website: https://cointopay.com/
repository: 
issue: 
icon: com.cointopay.iosapp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-22
signer: 
reviewArchive: 
twitter: Cointopay
social:
- https://www.facebook.com/CointopayInternational
- https://www.reddit.com/r/Cointopay

---

{% include copyFromAndroid.html %}
