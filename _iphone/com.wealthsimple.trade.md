---
wsId: WealthsimpleTrade
title: Wealthsimple Trade & Crypto
altTitle: 
authors:
- danny
appId: com.wealthsimple.trade
appCountry: ca
idd: 1403491709
released: 2019-02-26
updated: 2022-03-21
version: 2.31.0
stars: 4.66972
reviews: 122969
size: '123497472'
website: https://www.wealthsimple.com/en-ca/product/trade/
repository: 
issue: 
icon: com.wealthsimple.trade.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-09-03
signer: 
reviewArchive: 
twitter: Wealthsimple
social:
- https://www.facebook.com/wealthsimple

---

{% include copyFromAndroid.html %}
