---
wsId: coinmetro
title: 'CoinMetro: Crypto Exchange'
altTitle: 
authors:
- danny
appId: app.coinmetro
appCountry: us
idd: 1397585225
released: 2018-07-25
updated: 2022-02-01
version: '44181'
stars: 2.86567
reviews: 67
size: '26108928'
website: https://coinmetro.com/
repository: 
issue: 
icon: app.coinmetro.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive: 
twitter: CoinMetro
social:
- https://www.linkedin.com/company/coinmetro
- https://www.facebook.com/CoinMetro

---

{% include copyFromAndroid.html %}
