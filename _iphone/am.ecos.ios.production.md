---
wsId: ECOS
title: Ecos BTC mining, crypto wallet
altTitle: 
authors:
- danny
appId: am.ecos.ios.production
appCountry: us
idd: 1528964374
released: 2020-11-25
updated: 2022-03-07
version: 1.22.6
stars: 3.18519
reviews: 27
size: '68130816'
website: https://ecos.am/
repository: 
issue: 
icon: am.ecos.ios.production.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-07
signer: 
reviewArchive: 
twitter: ecosmining
social:
- https://www.facebook.com/ecosdefi

---

{% include copyFromAndroid.html %}
