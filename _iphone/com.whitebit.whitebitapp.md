---
wsId: whitebit
title: WhiteBIT – buy & sell bitcoin
altTitle: 
authors:
- danny
appId: com.whitebit.whitebitapp
appCountry: ua
idd: 1463405025
released: 2019-05-21
updated: 2022-03-20
version: 2.12.0
stars: 4.68844
reviews: 199
size: '155669504'
website: https://whitebit.com
repository: 
issue: 
icon: com.whitebit.whitebitapp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-10
signer: 
reviewArchive: 
twitter: whitebit
social:
- https://www.linkedin.com/company/whitebit-cryptocurrency-exchange
- https://www.facebook.com/whitebit
- https://www.reddit.com/r/WhiteBitExchange

---

{% include copyFromAndroid.html %}