---
wsId: Moneybrain
title: Moneybrain P2P Digital Banking
altTitle: 
authors:
- danny
appId: com.moneybrain.moneybrain
appCountry: gb
idd: 1476827262
released: 2019-10-15
updated: 2022-02-14
version: 2.0.6
stars: 4.85714
reviews: 7
size: '34324480'
website: https://bips.moneybrain.com
repository: 
issue: 
icon: com.moneybrain.moneybrain.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-22
signer: 
reviewArchive: 
twitter: MoneybrainBiPS
social: 

---

{% include copyFromAndroid.html %}