---
wsId: CoinBerry
title: Coinberry Bitcoin Wallet App
altTitle: 
authors:
- danny
appId: com.coinberry.coinberry
appCountry: ca
idd: 1370601820
released: 2018-06-09
updated: 2022-03-17
version: '126.00'
stars: 4.48808
reviews: 5542
size: '50945024'
website: https://coinberry.com
repository: 
issue: 
icon: com.coinberry.coinberry.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: CoinberryHQ
social:
- https://www.linkedin.com/company/coinberry
- https://www.facebook.com/CoinberryOfficial

---

{% include copyFromAndroid.html %}
