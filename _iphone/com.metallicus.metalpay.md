---
wsId: MetalPay
title: Metal Pay
altTitle: 
authors:
- danny
appId: com.metallicus.metalpay
appCountry: us
idd: 1345101178
released: 2018-09-14
updated: 2022-02-28
version: 2.8.2
stars: 4.29366
reviews: 4100
size: '127497216'
website: https://metalpay.com
repository: 
issue: 
icon: com.metallicus.metalpay.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive: 
twitter: metalpaysme
social:
- https://www.facebook.com/metalpaysme
- https://www.reddit.com/r/MetalPay

---

{% include copyFromAndroid.html %}

