---
wsId: buda
title: Buda
altTitle: 
authors:
- leo
appId: com.buda.crypto
appCountry: 
idd: 1321460860
released: 2018-01-04
updated: 2022-03-15
version: 2.0.20
stars: 5
reviews: 3
size: '71685120'
website: 
repository: 
issue: 
icon: com.buda.crypto.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-01-16
signer: 
reviewArchive: 
twitter: BudaPuntoCom
social:
- https://www.linkedin.com/company/budapuntocom
- https://www.facebook.com/BudaPuntoCom

---

This app has very poor ratings on Google (2.5 stars) and Apple (3.5 stars),
mainly due to limited functionality and high fees. Caution is advised!

This app is an interface to an exchange and coins are held there and not on the
phone. As a custodial service it is **not verifiable**.
