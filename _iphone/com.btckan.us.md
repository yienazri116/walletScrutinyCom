---
wsId: BitKan
title: 'BitKan: Trade Bitcoin & Crypto'
altTitle: 
authors:
- danny
appId: com.btckan.us
appCountry: us
idd: 1004852205
released: 2015-06-24
updated: 2022-01-12
version: 8.2.1
stars: 4.68492
reviews: 73
size: '180712448'
website: https://bitkan.com/
repository: 
issue: 
icon: com.btckan.us.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive: 
twitter: bitkanofficial
social: 

---

{% include copyFromAndroid.html %}
