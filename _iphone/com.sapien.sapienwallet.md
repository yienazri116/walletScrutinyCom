---
wsId: SapienWallet
title: Sapien Wallet
altTitle: 
authors:
- danny
appId: com.sapien.sapienwallet
appCountry: us
idd: 1529912521
released: 2021-06-21
updated: 2022-01-27
version: '1.17'
stars: 5
reviews: 2
size: '240642048'
website: https://sapienwallet.com/
repository: 
issue: 
icon: com.sapien.sapienwallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-11-10
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/SapienWallet

---

{% include copyFromAndroid.html %}
