---
wsId: wazirx
title: 'WazirX: Buy Bitcoin & Crypto'
altTitle: 
authors: 
appId: com.wrx.wazirx
appCountry: in
idd: 1349082789
released: 2018-03-07
updated: 2022-02-26
version: '1.15'
stars: 4.25549
reviews: 44242
size: '35561472'
website: https://wazirx.com
repository: 
issue: 
icon: com.wrx.wazirx.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-08-09
signer: 
reviewArchive: 
twitter: WazirxIndia
social:
- https://www.linkedin.com/company/wazirx
- https://www.facebook.com/wazirx

---

As this exchange allows holding your BTC in the app such
as sending and receiving them, it is usable as a wallet. A custodial wallet. As
such it is **not verifiable**.
