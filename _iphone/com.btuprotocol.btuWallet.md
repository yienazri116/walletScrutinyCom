---
wsId: Verso
title: 'Verso Wallet : Get into crypto'
altTitle: 
authors:
- danny
appId: com.btuprotocol.btuWallet
appCountry: ba
idd: 1539304605
released: 2021-03-15
updated: 2022-03-19
version: 2.5.2
stars: 0
reviews: 0
size: '90003456'
website: https://get-verso.com
repository: 
issue: 
icon: com.btuprotocol.btuWallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-10-19
signer: 
reviewArchive: 
twitter: versoapp
social: 

---

{% include copyFromAndroid.html %}
