---
wsId: FlareWallet
title: Flare Wallet
altTitle: 
authors: 
appId: org.flarewallet.flare
appCountry: 
idd: 1496651406
released: 2020-02-11
updated: 2021-03-13
version: 1.4.0
stars: 3.94444
reviews: 36
size: '24013824'
website: https://flarewallet.io
repository: 
issue: 
icon: org.flarewallet.flare.jpg
bugbounty: 
meta: stale
verdict: nosource
date: 2022-03-11
signer: 
reviewArchive: 
twitter: 
social: 

---

 {% include copyFromAndroid.html %}

Old Review
---

**Update:** We did not get to review this app before it was removed from the App
Store.
