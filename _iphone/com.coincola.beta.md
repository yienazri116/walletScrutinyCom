---
wsId: coincola
title: CoinCola – Buy Bitcoin
altTitle: 
authors:
- leo
appId: com.coincola.beta
appCountry: 
idd: 1234231551
released: 2017-06-06
updated: 2022-01-27
version: 4.8.9
stars: 3.66667
reviews: 225
size: '150128640'
website: https://www.coincola.com
repository: 
issue: 
icon: com.coincola.beta.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-24
signer: 
reviewArchive: 
twitter: CoinCola_Global
social:
- https://www.linkedin.com/company/coincola
- https://www.facebook.com/CoinCola
- https://www.reddit.com/r/coincolaofficial

---

> SAFE AND SECURE<br>
  Our team uses bank-level encryption, cold storage and SSL for the highest
  level of security.

Cold storage has only a meaning in the context of a custodial app. As such it
is **not verifiable**.
