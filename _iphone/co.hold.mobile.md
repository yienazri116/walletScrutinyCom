---
wsId: coholdmobile
title: HOLD — Buy Bitcoin & Crypto
altTitle: 
authors:
- leo
appId: co.hold.mobile
appCountry: de
idd: 1435187229
released: 2018-09-28
updated: 2022-01-21
version: 3.15.0
stars: 3.90476
reviews: 21
size: '33532928'
website: https://hold.io
repository: 
issue: 
icon: co.hold.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-03-11
signer: 
reviewArchive: 
twitter: HoldHQ
social:
- https://www.linkedin.com/company/holdhq
- https://www.facebook.com/HoldHQ

---

> SAFETY FIRST<br>
  Regulated and licensed in the EU. Your money is securely held by banks within
  the European Union and your crypto protected by the world-renowned custodian
  BitGo.

This app is a custodial offering and thus **not verifiable**.

