---
wsId: metamask
title: MetaMask - Blockchain Wallet
altTitle: 
authors:
- leo
appId: io.metamask.MetaMask
appCountry: 
idd: 1438144202
released: 2020-09-03
updated: 2022-03-08
version: 4.2.2
stars: 4.54277
reviews: 17044
size: '53046272'
website: https://metamask.io/
repository: 
issue: 
icon: io.metamask.MetaMask.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-05-01
signer: 
reviewArchive: 
twitter: 
social: 

---

This is an ETH-only app and thus not a Bitcoin wallet.
