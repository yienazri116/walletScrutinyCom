---
wsId: AMarkets
title: AMarkets Trading
altTitle: 
authors:
- danny
appId: amarkets.app
appCountry: us
idd: 1495820700
released: 2020-02-12
updated: 2022-03-19
version: 1.4.34
stars: 4.48571
reviews: 35
size: '76178432'
website: https://www.amarkets.com/
repository: 
issue: 
icon: amarkets.app.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-10-13
signer: 
reviewArchive: 
twitter: 
social:
- https://www.linkedin.com/company/amarkets
- https://www.facebook.com/AMarketsFirm

---

{% include copyFromAndroid.html %}
