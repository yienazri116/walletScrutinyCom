---
wsId: dCent
title: D’CENT Wallet
altTitle: 
authors:
- danny
appId: kr.iotrust.dcent
appCountry: kr
idd: 1447206611
released: 2019-01-26
updated: 2022-03-16
version: 5.13.5
stars: 4.02083
reviews: 48
size: '44776448'
website: https://dcentwallet.com/
repository: 
issue: 
icon: kr.iotrust.dcent.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2022-01-10
signer: 
reviewArchive: 
twitter: DCENTwallets
social:
- https://www.facebook.com/DcentWalletGlobal
- https://github.com/DcentWallet

---

{% include copyFromAndroid.html %}